namespace java src.main.java.protos.thrift

struct User {
  1: string name,
  2: i32 age
}

service UserService {
  User find()
}
