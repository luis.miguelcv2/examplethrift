package thrift;

import org.apache.thrift.TException;
import src.main.java.protos.thrift.UserService.Iface;
import src.main.java.protos.thrift.User;

public class UserServiceImpl implements Iface {
    @Override
    public User find() throws TException {
        User user = new User();
        user.setName("HenryXi");
        user.setAge(27);
        return user;
    }
}